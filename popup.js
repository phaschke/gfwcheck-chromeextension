document.addEventListener('DOMContentLoaded', function() {

  document.getElementById("testIpButton").onclick = onVerifyButtonClick;

}, false);

function onVerifyButtonClick() {

  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, function(tabs) {

    chrome.tabs.sendMessage(tabs[0].id, {
      text: 'request_ip'
    }, function(response) {

      if (response.result === 1) {

        var ipAddress = response.value;

        if (ipAddress == "" || ipAddress == undefined) {

          document.getElementById("result").innerHTML = "<p><i>Please enter an IP address.</i></p>";

        } else {

          document.getElementById("ipAddress").innerHTML = "<p>" + response.value + "</p>";

          document.getElementById("result").innerHTML = "<p>Working...</p>";

          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

              var resultPayload = JSON.parse(this.responseText);
              /*var resultPayload = {
                //{"result":"error","error":"IP Error"};
                "result": "success",
                "data": {
                  "outside_gfw": {
                    "icmp": {
                      "accepted": true,
                      "alive": false
                    },
                    "tcp": {
                      "accepted": true,
                      "alive": true
                    }
                  },
                  "inside_gfw": {
                    "icmp": {
                      "accepted": true,
                      "alive": true
                    },
                    "tcp": {
                      "accepted": true,
                      "alive": false
                    }
                  },
                  "reports": 4,
                  "total_reports": 4,
                  "processing_time": 1799
                }
              };*/

              var successImage = chrome.runtime.getURL("success-32.png");
              var failImage = chrome.runtime.getURL("fail-32.png");

              var tcpOutside = '<img class="statusImage" src="' + failImage + '" />';
              var tcpInside = '<img class="statusImage" src="' + failImage + '" />';
              var icmpOutside = '<img class="statusImage" src="' + failImage + '" />';
              var icmpInside = '<img class="statusImage" src="' + failImage + '" />';

              console.log(resultPayload.result);
              if (resultPayload.result == "success") {
                var outsideGFW = resultPayload.data.outside_gfw;
                var insideGFW = resultPayload.data.inside_gfw;
                if (outsideGFW.tcp.accepted && outsideGFW.tcp.alive) {
                  tcpOutside = '<img class="statusImage" src="' + successImage + '" />';
                }
                if (outsideGFW.icmp.accepted && outsideGFW.icmp.alive) {
                  icmpOutside = '<img class="statusImage" src="' + successImage + '" />';
                }
                if (insideGFW.tcp.accepted && insideGFW.tcp.alive) {
                  tcpInside = '<img class="statusImage" src="' + successImage + '" />';
                }
                if (insideGFW.icmp.accepted && insideGFW.icmp.alive) {
                  icmpInside = '<img class="statusImage" src="' + successImage + '" />';
                }

                // Generate results table

                var table = "<table id='resultsTable'>" +
                  "<tr>" +
                  "<th></th>" +
                  "<th>Outside GFW</th>" +
                  "<th>Inside GFW</th>" +
                  "</tr>" +
                  "<tr>" +
                  "<th>TCP</th>" +
                  "<td>" + tcpOutside + "</td>" +
                  "<td>" + tcpInside + "</td>" +
                  "</tr>" +
                  "<tr>" +
                  "<th>ICMP</th>" +
                  "<td>" + icmpOutside + "</td>" +
                  "<td>" + icmpInside + "</td>" +
                  "</tr>" +
                  "</table>";

                document.getElementById("result").innerHTML = table;

              } else {
                //Error
                document.getElementById("result").innerHTML = "<b><p class='red'>IP Check Failed</b><br> Reason: " + resultPayload.error + "</p>";
              }
            }
          };
          xhttp.open("GET", "https://ipcheck.need.sh/api_v2.php?ip=" + ipAddress, true);
          xhttp.send();



        }

      } else {

        // Error
        document.getElementById("result").innerHTML = "<p>" + response.message + "</p>";

      }
    });
  });

}
