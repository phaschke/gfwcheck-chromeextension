chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {

    if (msg.text === 'request_ip') {

        var ipFields = document.getElementsByName("dedicatedip");

        if (ipFields.length > 0) {

            sendResponse({
                "result": 1,
                "value": ipFields[0].value
            });

        } else {

            sendResponse({
                "result": 0,
                "message": "Could not find IP field on the page."
            });

        }
    }
});
